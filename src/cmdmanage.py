#!/usr/bin/env python3

# internal
from vapi import auth, get, post, getprojectid, getstackid, getimages, getstackvmids

def run(option, action, targetname, projectname='none', stackname='none', count=0):
	if option == 'project':
		print('not implemented')
	
	if option == 'stack':
		if projectname == 'none':
			print('please specify project name for the stack')
			return
		stackname = targetname
		projectid = getprojectid(projectname)
		stackid = getstackid(stackname, projectname)
		vmids =  getstackvmids(stackname, projectname)

		if action == 'poweron':
			print('not implemented')
		if action == 'poweroff':
			print('not implemented')
		if action == 'reset':
			print('not implemented')
		if action == 'scaleup':
			if count == 0:
				print('please specify the instance count for the stack')
				return
			print('not implemented')
		if action == 'scaledown':
			if count == 0:
				print('please specify the instance count for the stack')
				return
			print('not implemented')

	if option == 'vm':
		if action == 'poweron':
			print('not implemented')
		if action == 'poweroff':
			print('not implemented')
		if action == 'reset':
			print('not implemented')

	if option == 'network':
		print('not implemented')


def getvmstate(vmid, stackid, projectid):
