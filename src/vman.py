#!/usr/bin/env python3

#external
import os
import sys
import argparse

#internal
import cmdlist
import cmdshow
import cmdadd
import cmdlaunch
import cmddelete

def main():
	main = argparse.ArgumentParser(description='vman provides openstack functionality without openstack bugs',
									epilog='please remember to export VC_USERNAME VC_PASSWORD and VC_HOSTNAME before running any commands')
	cmd = main.add_subparsers(help='', dest='cmd')
	cmd_list = cmd.add_parser('list', help='list all resources of a given type')
	cmd_show = cmd.add_parser('show', help='show availale resources in a parent object')
	cmd_add = cmd.add_parser('add', help='create projects, stacks, vms, and networks')
	cmd_launch = cmd.add_parser('launch', help='launch a new stack')
	cmd_manage = cmd.add_parser('manage', help='manage an existing resource')
	cmd_delete = cmd.add_parser('delete', help='delete all or individual projects, stacks, vms, and networks')
	cmd_launch_opt = cmd_launch.add_subparsers(help='', dest='opt')
	cmd_launch_opt_stack = cmd_launch_opt.add_parser('stack', help='launch a new stack')
	cmd_manage_opt = cmd_manage.add_subparsers(help='', dest='opt')
	cmd_manage_opt_project = cmd_manage_opt.add_parser('project', help='manage an existing project')
	cmd_manage_opt_stack = cmd_manage_opt.add_parser('stack', help='manage an existing stack')
	cmd_manage_opt_vm = cmd_manage_opt.add_parser('vm', help='manage an existing vm')
	cmd_manage_opt_network = cmd_manage_opt.add_parser('network', help='manage an existing network')
	cmd_show_opt = cmd_show.add_subparsers(help='', dest='opt')
	cmd_show_opt_project = cmd_show_opt.add_parser('project', help='show all stacks, vms, ips, or hostnames in a project')
	cmd_show_opt_stack = cmd_show_opt.add_parser('stack', help='show all vms, ips, or hostnames in a stack')
	cmd_show_opt_vm = cmd_show_opt.add_parser('vm', help='show ip or hostname assigned to a vm')
	cmd_show_opt_network = cmd_show_opt.add_parser('network', help='show ips or hostnames connected to the network, or display configured vlan')
	cmd_add_opt = cmd_add.add_subparsers(help='', dest='opt')
	cmd_add_opt_project = cmd_add_opt.add_parser('project', help='add new project to the environment')
	cmd_add_opt_stack = cmd_add_opt.add_parser('stack', help='add new stack to a project')
	cmd_add_opt_vm = cmd_add_opt.add_parser('vm', help='add new vm to a stack')
	cmd_add_opt_network = cmd_add_opt.add_parser('network', help='add new network to the environment')
	cmd_delete_opt = cmd_delete.add_subparsers(help='', dest='opt')
	cmd_delete_opt_all = cmd_delete_opt.add_parser('all', help='delete all resources of a given type - projects, stacks, vms, or networks')
	cmd_delete_opt_project = cmd_delete_opt.add_parser('project', help='delete a project from the environment')
	cmd_delete_opt_stack = cmd_delete_opt.add_parser('stack', help='delete a stack from a project')
	cmd_delete_opt_vm = cmd_delete_opt.add_parser('vm', help='delete a vm from a stack')
	cmd_delete_opt_network = cmd_delete_opt.add_parser('network', help='delete a network from the environment')

	cmd_list.add_argument('resource', choices=['projects', 'stacks', 'vms', 'ips', 'hostnames', 'networks', 'images'], help='resource type to list', default='none')
	cmd_launch_opt_stack.add_argument('--project', help='project for the new stack', default='none')
	cmd_launch_opt_stack.add_argument('--count', help='instance count', default='none')
	cmd_launch_opt_stack.add_argument('--image', help='image to use for the vms', default='none')
	cmd_launch_opt_stack.add_argument('name', help='stack name', default='none')
	#cmd_manage_opt_project.add_argument('name', help='name of the target project', default='none')
	#cmd_manage_opt_project.add_argument('action', help='action to take against a project', default='none', choices=[''])
	cmd_manage_opt_stack.add_argument('action', help='action to take against a stack', default='none', choices=['poweron', 'poweroff', 'reset', 'scaledown', 'scaleup'])
	cmd_manage_opt_stack.add_argument('name', help='name of the target stack', default='none')
	cmd_manage_opt_stack.add_argument('--project', help='project to which target stack belongs', default='none')
	cmd_manage_opt_stack.add_argument('--count', help='target instance count', default='none')
	cmd_manage_opt_vm.add_argument('--project', help='project to which target vm belongs', default='none')
	cmd_manage_opt_vm.add_argument('--stack', help='stack to which target vm belongs', default='none')
	cmd_manage_opt_vm.add_argument('name', help='name of the target vm', default='none')
	cmd_manage_opt_vm.add_argument('action', help='action to take against a vm', default='none', choices=['poweron', 'poweroff', 'reset'])
	#cmd_manage_opt_network.add_argument('name', help='name of the target network', default='none')
	#cmd_manage_opt_network.add_argument('action', help='action to take against a network', default='none', choices=[''])
	cmd_show_opt_project.add_argument('name', help='project name', default='none')
	cmd_show_opt_project.add_argument('resource', help='choose resource type to show', choices=['stacks', 'vms', 'ips', 'hostnames'], default='none')
	cmd_show_opt_stack.add_argument('--project', help='project to which target stack belongs', default='none')
	cmd_show_opt_stack.add_argument('name', help='stack name', default='none')
	cmd_show_opt_stack.add_argument('resource', help='choose resource type to show', choices=['vms', 'ips', 'hostnames'], default='none')
	cmd_show_opt_vm.add_argument('--project', help='project to which target vm belongs', default='none')
	cmd_show_opt_vm.add_argument('--stack', help='stack to which target vm belongs', default='none')
	cmd_show_opt_vm.add_argument('name', help='vm name', default='none')
	cmd_show_opt_vm.add_argument('resource', help='choose resource type to show', choices=['ip', 'hostname'], default='none')
	cmd_show_opt_network.add_argument('name', help='network name', default='none')
	cmd_show_opt_network.add_argument('resource', help='choose resource type to show', choices=['vms', 'ips', 'hostnames', 'vlan'], default='none')
	cmd_add_opt_project.add_argument('name', help='desired project name', default='none')
	cmd_add_opt_stack.add_argument('--project', help='target project', default='none')
	cmd_add_opt_stack.add_argument('name', help='desired stack name', default='none')
	cmd_add_opt_vm.add_argument('--project', help='target project', default='none')
	cmd_add_opt_vm.add_argument('--stack', help='target stack', default='none')
	cmd_add_opt_vm.add_argument('name', help='desired vm name', default='none')
	cmd_add_opt_vm.add_argument('--image', help='image to use for the vm', default='none')
	cmd_add_opt_network.add_argument('name', help='desired network name', default='none')
	cmd_add_opt_network.add_argument('--vlan', help='vlan tag for the network', default='none')
	cmd_delete_opt_all.add_argument('object', help='resource type to delete', choices=['projects', 'stacks', 'vms', 'networks'], default='none')
	cmd_delete_opt_all.add_argument('--project', help='target project (required for stacks and vms)', default='none')
	cmd_delete_opt_all.add_argument('--stack', help='target stack (required for vms)', default='none')
	cmd_delete_opt_project.add_argument('name', help='project to be deleted', default='none')
	cmd_delete_opt_stack.add_argument('--project', help='project to which target stack belongs', default='none')
	cmd_delete_opt_stack.add_argument('name', help='stack to be deleted', default='none')
	cmd_delete_opt_vm.add_argument('--project', help='project to which the vm belongs', default='none')
	cmd_delete_opt_vm.add_argument('--stack', help='stack to which the vm belongs', default='none')
	cmd_delete_opt_vm.add_argument('name', help='vm to be deleted', default='none')
	cmd_delete_opt_network.add_argument('name', help='network to be deleted', default='none')

	args = main.parse_args()
	
	if validatevariables():
		if args.cmd == 'list':
			cmdlist.run(args.resource)
		if args.cmd == 'launch':
			if args.opt == 'stack':
				cmdlaunch.run(args.name, args.project, 'none', args.count, args.image)
			if args.opt == 'instance':
				cmdadd.run('vm', args.name, args.project, args.stack, 'none', args.image)
		if args.cmd == 'manage':
			if args.opt == 'project':
				print('not implemented')
			if args.opt == 'stack':
				print('not implemented')
			if args.opt == 'vm':
				print('not implemented')
			if args.opt == 'network':
				print('not implemented')
		if args.cmd == 'show':
			if args.opt == 'project':
				cmdshow.run(args.opt, args.name, args.resource)
			if args.opt == 'stack':
				cmdshow.run(args.opt, args.name, args.resource, args.project)
			if args.opt == 'vm':
				cmdshow.run(args.opt, args.name, args.resource, args.project, args.stack)
			if args.opt == 'network':
				cmdshow.run(args.opt, args.name, args.resource)
		if args.cmd == 'add':
			if args.opt == 'project':
				cmdadd.run(args.opt, args.name)
			if args.opt == 'stack':
				cmdadd.run(args.opt, args.name, args.project)
			if args.opt == 'vm':
				cmdadd.run(args.opt, args.name, args.project, args.stack, 'none', args.image)
			if args.opt == 'network':
				cmdadd.run(args.opt, args.name, args.vlan)
		if args.cmd == 'delete':
			if args.opt == 'all':
				cmddelete.run(args.opt, args.object, args.project, args.stack)
			if args.opt == 'project':
				cmddelete.run(args.opt, args.name)
			if args.opt == 'stack':
				cmddelete.run(args.opt, args.name, args.project)
			if args.opt == 'vm':
				cmddelete.run(args.opt, args.name, args.project, args.stack)
			if args.opt == 'network':
				cmddelete.run(args.opt, args.name, args.vlan)


def validatevariables():
	try:
		username = os.environ["VC_USERNAME"]
		password = os.environ["VC_PASSWORD"]
		vcenter = os.environ["VC_HOSTNAME"]
	except KeyError:
		print('please export VC_USERNAME VC_PASSWORD and VC_HOSTNAME before using vman')
		return False
	return True


# this bit starts main() function and handles interruptions
if __name__ == '__main__':
	main()
	#try:
	#	main()
	#except:
	#	sys.exit(0)	



#### BUILD WITH
#### pyinstaller --onefile --console src/vman.py --hiddenimport=os --hiddenimport=sys --hiddenimport=argparse --hiddenimport=json --hiddenimport=requests --hiddenimport=urllib3