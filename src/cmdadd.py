#!/usr/bin/env python3

# internal
from vapi import auth, get, post, getprojectid, getimages


def run(object, name, project='none', stack='none', vlan='none', template='none'):

	auth()

	if object == 'project':
		payload = { 'associable_types': ['VirtualMachine'], 'cardinality': 'SINGLE', 'description': 'created by vman', 'name': f'vman-{name}' }
		result = post('/api/cis/tagging/category', payload)
		if result == 201:
			print(f'{name} project created')
		if result == 400:
			print('project name already exists')
			
	if object == 'stack':
		if project == 'none':
			print('please specify project name for the stack')
			return
		projectid = getprojectid(project)
		if projectid:
			payload = {'category_id': projectid, 'description': 'created by vman', 'name': f'vman-{name}'}
			result = post('/api/cis/tagging/tag', payload)
			if result == 201:
				print(f'{name} stack created')
			if result == 400:
				print('stack name already exists')
		else:
			print(f'invalid project name')

	if object == 'vm':
		if project == 'none':
			print('please specify project name for the vm')
			return
		if stack == 'none':
			print('please specify stack name for the vm')
			return
		if template == 'none':
			print('please specify image name for the vm')
			return
		images = getimages()
		for image in images:
			imagename = get(f'/api/vcenter/vm/{image["id"]}')["name"]
			if template == imagename:
				templateid = image["id"]
		payload = { "source": templateid, "name": name}
		post('/api/vcenter/vm?action=clone', payload)

	if object == 'network':
		print('not implemented')