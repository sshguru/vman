#!/usr/bin/env python3

# internal
from vapi import auth, get, post, getstackid, getstacks, getprojects, getimages


def run(object):
	
	auth()

	if object == 'images':
		imagenames = []
		images = getimages()
		for image in images:	
			imagename = get(f'/api/vcenter/vm/{image["id"]}')["name"]
			imagenames.append(imagename)
		print(imagenames)

	if object == 'projects':
		projects = []
		projectids = getprojects()
		for projectid in projectids:
			projects.append(projectid["name"].replace('vman-',''))
		projects = sorted(projects)
		print(f'{projects}')

	if object == 'stacks':
		stacks = []
		stackids = getstacks()
		for stackid in stackids:
			stacks.append(stackid["name"].replace('vman-',''))
		stacks = sorted(stacks)
		print(f'{stacks}')

	if object == 'vms':
		vms = []
		stackids = getstacks()
		for stackid in stackids:
			objectids = post(f'/api/cis/tagging/tag-association/{stackid["id"]}?action=list-attached-objects')
			vmids = []
			for objectid in objectids:
				vmids.append(objectid["id"])
			for vmid in vmids:
				vm = get(f'/api/vcenter/vm/{vmid}')["name"]
				vms.append(vm)
		vms = sorted(vms)
		print(f'{vms}')

	if object == 'ips':
		ips = []
		stackids = getstacks()
		for stackid in stackids:
			objectids = post(f'/api/cis/tagging/tag-association/{stackid["id"]}?action=list-attached-objects')
			vmids = []
			for objectid in objectids:
				vmids.append(objectid["id"])
			for vmid in vmids:
				try:
					ip = get(f'/api/vcenter/vm/{vmid}/guest/identity')["ip_address"]
					ips.append(ip)
				except:	
					vmname = get(f'/api/vcenter/vm/{vmid}')["name"]
					print(f'VM not reporting its IP: {vmname}')
		print(f'{ips}')

	if object == 'hostnames':
		hostnames = []
		stackids = getstacks()
		for stackid in stackids:
			objectids = post(f'/api/cis/tagging/tag-association/{stackid["id"]}?action=list-attached-objects')
			vmids = []
			for objectid in objectids:
				vmids.append(objectid["id"])	
			for vmid in vmids:
				try:
					hostname = get(f'/api/vcenter/vm/{vmid}/guest/identity')["host_name"]
					hostnames.append(hostname)
				except:
					vmname = get(f'/api/vcenter/vm/{vmid}')["name"]
					print(f'VM not reporting its hostname: {vmname}')
		hostnames = sorted(hostnames)
		print(f'{hostnames}')
		
	if object == 'networks':
		tag = getstackid('network','vman',False)
		networks = []
		networkids = post(f'/api/cis/tagging/tag-association/{tag}?action=list-attached-objects')
		allnetworkids = get('/api/vcenter/network?types=DISTRIBUTED_PORTGROUP')
		for networkid in networkids:
			for allnetworkid in allnetworkids:
				if allnetworkid["network"] == networkid["id"]:
					networks.append(allnetworkid["name"])
		networks = sorted(networks)
		print(networks)