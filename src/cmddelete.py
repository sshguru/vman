#!/usr/bin/env python3

# internal
from vapi import auth, get, post, delete, getprojectid, getstackid, getvmid, findstack


def run(object, name, project='none', stack='none'):

	auth()

	if object == 'all':
		if name == 'projects':
			projects = []
			projectids = get('/api/cis/tagging/category')
			vmanprojects = []
			for projectid in projectids:
				projectname = get(f'/api/cis/tagging/category/{projectid}')["name"]
				if projectname.startswith('vman-'):
					vmanprojects.append(projectid)
			if vmanprojects:
				for vmanprojectid in vmanprojects:
					delete(f'/api/cis/tagging/category/{vmanprojectid}')
				print('all projects delted')
			else:
				print('no projects in place')

		if name == 'stacks':
			if project == 'none':
				print('please specify project name for the stacks')
				return
			stacks = []
			projectid = getprojectid(project)
			payload = {'category_id': projectid}
			stackids = post('/api/cis/tagging/tag?action=list-tags-for-category', payload)
			if stackids:
				for stackid in stackids:
					stackname = get(f'/api/cis/tagging/tag/{stackid}')["name"]
					if stackname.startswith('vman-'):
						delete(f'/api/cis/tagging/tag/{stackid}')
				print(f'all stacks delted in project {project}')
			else:
				print(f'project {project} contains no stacks') 

		if name == 'vms':
			if project == 'none':
				print('please specify project name for the vms')
				return
			if stack == 'none':
				print('please specify stack name for the vms')
				return
			stackid = getstackid(stack, project)
			vmids = post(f'/api/cis/tagging/tag-association/{stackid["id"]}?action=list-attached-objects')
			for vmid in vmids:
				delete(f'/api/vcenter/vm/{vmid["id"]}')

		if name == 'networks':
			print('not implemented')

	if object == 'project':
		projectid = getprojectid(name)
		result = delete(f'/api/cis/tagging/category/{projectid}')
		if result == 200:
			print(f'{name} project deleted')
		if result == 404:
			print("project name doesn't exists")

	if object == 'stack':
		if project == 'none':
			print('please specify project name for the stack')
			return
		if not findstack(name, project):
			print('stack not found')
			return False
		stackid = getstackid(name, project)
		result = delete(f'/api/cis/tagging/tag/{stackid}')
		if result == 200:
			print(f'{name} stack deleted')
		if result == 404:
			print("stack name doesn't exists")

	if object == 'vm':
		if project == 'none':
			print('please specify project name for the vm')
			return
		if stack == 'none':
			print('please specify stack name for the vm')
			return
		vmid = getvmid(name, stack, project)
		delete(f'/api/vcenter/vm/{vmid}')

	if object == 'network':
		print('not implemented')