#!/usr/bin/env python3

# internal
from vapi import auth, get, post, getprojectid, getimages

def run(stackname, projectname='none', instancecount=0, imagename='none'):
	if project == 'none':
		print('please specify project name for the stack')
		return
	if imagename == 'none':
		print('please specify image name for the vms in the stack')
		return
	if instancecount == 0:
		print('please specify the instance count for the stack')
		return
	projectid = getprojectid(projectname)
	imageid = getimageid(imagename)
	for i in instancecount:
		instancename = f'stackname{i}'
		payload = { "source": imageid, "name": instancename}
		post('/api/vcenter/vm?action=clone', payload)