#!/usr/bin/env python3

# external
import os
import json
import requests
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


username = os.environ["VC_USERNAME"]
password = os.environ["VC_PASSWORD"]
vcenter = os.environ["VC_HOSTNAME"]
vapi = f'https://{vcenter}'
session = requests.Session()


def auth():
	session.verify = False
	response = session.post(f'{vapi}/rest/com/vmware/cis/session', auth=(username, password))
	if response.status_code != 200:
		print(f'HTTP error: {response.status_code}')
		return
	return session


def get(url):
	response = session.get(f'{vapi}{url}')
	if response.status_code != 200:
		print(f'HTTP error: {response.status_code}')
		return response.status_code
	return json.loads(response.text)


def post(url, payload='none'):
	if payload == 'none':
		response = session.post(f'{vapi}{url}')
	else:
		response = session.post(f'{vapi}{url}', json=payload)
	if response.status_code != 200 and response.status_code != 201:
		print(f'HTTP error: {response.status_code}')
		return response.status_code
	if response.status_code == 201:
		return response.status_code
	return json.loads(response.text)


def delete(url):
	response = session.delete(f'{vapi}{url}')
	if response.status_code != 204:
		print(f'HTTP error: {response.status_code}')
		return response.status_code
	return 200


def getprojectid(name, vman=True):
	projectids = get('/api/cis/tagging/category')
	for projectid in projectids:
		project = get(f'/api/cis/tagging/category/{projectid}')
		projectname = project["name"]
		if vman:
			if projectname.startswith('vman-'):
				if projectname.replace('vman-','') == name:
					return project["id"]
		else:
			if projectname == name:
				return project["id"]
	return


def getstackid(targetstackname, projectname, vman=True):
	projectid = getprojectid(projectname, vman)
	payload = { 'category_id': projectid }
	stackids = post('/api/cis/tagging/tag?action=list-tags-for-category', payload)
	for stackid in stackids:
		stackobj = get(f'/api/cis/tagging/tag/{stackid}')
		stackname = stackobj["name"]
		if vman:
			if stackname.startswith('vman-'):
				if stackname.replace('vman-','') == targetstackname:
					return stackobj["id"]
		else:
			if stackname == targetstackname:
				return stackobj["id"]


def getvmid(name, stack, project):
	stackid = getstackid(stack, project)
	vmobjects = post(f'/api/cis/tagging/tag-association/{stackid}?action=list-attached-objects')
	for vmobject in vmobjects:
		vmname = get(f'/api/vcenter/vm/{vmobject["id"]}')["name"]
		if vmname == name:
			return vmobject["id"]


def getimageid(name):
	images = getimages()
	for image in images:
		imagename = get(f'/api/vcenter/vm/{image["id"]}')["name"]
		if template == imagename:
			return image["id"]


def findstack(name, project):
	projectid = getprojectid(project)
	stacks = []
	payload = { 'category_id': projectid }
	stackids = post('/api/cis/tagging/tag?action=list-tags-for-category', payload)
	for stackid in stackids:
		stack = get(f'/api/cis/tagging/tag/{stackid}')["name"]
		if stack.startswith('vman-'):
			if stack.replace('vman-','') == name:
				return True
	return False


def findvm(name, stack, project):
	stackid = getstackid(stack, project)
	vms = []
	objectids = post(f'/api/cis/tagging/tag-association/{stackid}?action=list-attached-objects')
	vmids = []
	for objectid in objectids:
		vmids.append(objectid["id"])
	for vmid in vmids:
		vmname = get(f'/api/vcenter/vm/{vmid}')["name"]
		if vmname == name:
			return True
	return False


def getprojects():
	projects = []
	projectids = get('/api/cis/tagging/category')
	for projectid in projectids:
		project = get(f'/api/cis/tagging/category/{projectid}')["name"]
		if project.startswith('vman-'):
			projects.append(get(f'/api/cis/tagging/category/{projectid}'))
	return projects


def getstacks():
	stacks = []
	stackids = get('/api/cis/tagging/tag')
	for stackid in stackids:
		stack = get(f'/api/cis/tagging/tag/{stackid}')["name"]
		if stack.startswith('vman-'):
			stacks.append(get(f'/api/cis/tagging/tag/{stackid}'))
	return stacks


def getprojectstackids(projectid):
	stacks = []
	payload = { 'category_id': projectid }
	stackids = post('/api/cis/tagging/tag?action=list-tags-for-category', payload)
	return stackids


def getstackvmids(stackname, projectname):
	projectid = getprojectid(projectname)
	stackid = getstackid(stackname, projectname)
	vmids = post(f'/api/cis/tagging/tag-association/{tag}?action=list-attached-objects')
	return vmids


def getimages():
	tag = getstackid('template','vman',False)
	images = []
	imageids = post(f'/api/cis/tagging/tag-association/{tag}?action=list-attached-objects')
	for imageid in imageids:
		images.append(imageid)
		return images