#!/usr/bin/env python3

# internal
from vapi import auth, get, post, getprojectid, getvmid, getstacks, getstackid, getprojectstackids, findstack, findvm


def run(object, targetname, resource, targetprojectname='none', targetstackname='none'):

	auth()

	if object == 'project':
		targetprojectname = targetname
		projectid = getprojectid(targetprojectname)
		if projectid == None:
			print('invalid project name')
			return
		stackids = getprojectstackids(projectid)
		if resource == "stacks":
			stacknames = []
			for stackid in stackids:
				stackname = get(f'/api/cis/tagging/tag/{stackid}')["name"]
				if stackname.startswith('vman-'):
					stacknames.append(stackname.replace('vman-',''))
			stacknames = sorted(stacknames)
			print(f'{stacknames}')
		if resource == "vms":
			vms = []
			for stackid in stackids:
				objectids = post(f'/api/cis/tagging/tag-association/{stackid}?action=list-attached-objects')
				vmids = []
				for objectid in objectids:
					vmids.append(objectid["id"])
				for vmid in vmids:
					vm = get(f'/api/vcenter/vm/{vmid}')["name"]
					vms.append(vm)
			vms = sorted(vms)
			print(f'{vms}')
		if resource == "ips":
			ips = []
			for stackid in stackids:
				objectids = post(f'/api/cis/tagging/tag-association/{stackid}?action=list-attached-objects')
				vmids = []
				for objectid in objectids:
					vmids.append(objectid["id"])
				for vmid in vmids:
					ip = get(f'/api/vcenter/vm/{vmid}/guest/identity')
					if ip != 503:
						ip = ip["ip_address"]
						ips.append(ip)
			ips = sorted(ips)
			if ips != []:
				print(f'{ips}')
			else:
				print('no ips detected')
		if resource == "hostnames":
			hostnames = []
			for stackid in stackids:
				objectids = post(f'/api/cis/tagging/tag-association/{stackid}?action=list-attached-objects')
				vmids = []
				for objectid in objectids:
					vmids.append(objectid["id"])
				for vmid in vmids:
					hostname = get(f'/api/vcenter/vm/{vmid}/guest/identity')
					if hostname != 503:
						hostname = hostname["host_name"]
						hostnames.append(hostname)
			hostnames = sorted(hostnames)
			if hostnames != []:
				print(f'{hostnames}')
			else:
				print('no hostnames detected')

	if object == 'stack':
		targetstackname = targetname
		if targetprojectname == 'none':
			print('please specify project name for the stack')
			return
		if not findstack(targetstackname, targetprojectname):
			print('stack not found')
			return False
		vmids = []
		stackid = getstackid(targetstackname, targetprojectname)
		objectids = post(f'/api/cis/tagging/tag-association/{stackid}?action=list-attached-objects')
		for objectid in objectids:
			vmids.append(objectid["id"])
		if resource == 'vms':
			vms = []
			for vmid in vmids:
				vm = get(f'/api/vcenter/vm/{vmid}')["name"]
				vms.append(vm)
			if vms != []:
				vms = sorted(vms)
				print(f'{vms}')
			else:
				print('no vms detected')
		if resource == 'ips':
			ips = []
			for vmid in vmids:
				ip = get(f'/api/vcenter/vm/{vmid}/guest/identity')["ip_address"]
				ips.append(ip)
			if ips != []:
				print(f'{ips}')
			else:
				print('no ips detected')
		if resource == 'hostnames':
			hostnames = []
			for vmid in vmids:
				hostname = get(f'/api/vcenter/vm/{vmid}/guest/identity')["host_name"]
				hostnames.append(hostname)
			if hostnames != []:
				hostnames = sorted(hostnames)
				print(f'{hostnames}')
			else:
				print('no hostnames detected')

	if object == 'vm':
		targetvmname = targetname
		if targetprojectname == 'none':
			print('please specify project name for the vm')
			return
		if targetstackname == 'none':
			print('please specify stack name for the vm')
			return
		if not findstack(targetstackname, targetprojectname):
			print('stack not found')
			return False
		if not findvm(targetvmname, targetstackname, targetprojectname):
			print('vm not found')
			return False
		vmid = getvmid(targetvmname, targetstackname, targetprojectname)
		if resource == "ip":
			ip = get(f'/api/vcenter/vm/{vmid}/guest/identity')
			if ip != 503:
				ip = ip["ip_address"]
				print(ip)
			else:
				print('no ip detected')
		if resource == "hostname":
			hostname = get(f'/api/vcenter/vm/{vmid}/guest/identity')
			if hostname != 503:
				hostname = hostname["host_name"]
				print(hostname)
			else:
				print('no hostname detected')

	if object == 'network':
		networkids = get('/api/vcenter/network?types=DISTRIBUTED_PORTGROUP')
		targetnetworkname = targetname
		network = None
		for networkid in networkids:
			networkname = networkid["name"]
			if networkname == targetnetworkname:
				network = networkid["network"]
		if network == None:
				print("network does not exist")
				return
		if resource == "vms" or resource == "ips" or resource == "hostnames":
			vms = []
			vmids = []
			stackids = getstacks()
			for stackid in stackids:
				objectids = post(f'/api/cis/tagging/tag-association/{stackid["id"]}?action=list-attached-objects')
				for objectid in objectids:
					vmids.append(objectid["id"])
				for vmid in vmids:
					vm = get(f'/api/vcenter/vm/{vmid}')
					if not isinstance(vm, int):
						vmnics = vm["nics"]
						for vmnic in vmnics:
							nicnetwork = vm["nics"][vmnic]["backing"]["network"]
							if nicnetwork == network:
								vms.append(vm) 
		if resource == "vms":
			vmnames = []
			for vm in vms:
				vmnames.append(vm["identity"]["name"])
			# remove duplicates - multiple NICs on VMs
			vmnames = list(dict.fromkeys(vmnames))
			vmanmes = sorted(vmnames)
			print(vmnames)
		if resource == "ips":
			ips = []
			for vmid in vmids:
				ip = get(f'/api/vcenter/vm/{vmid}/guest/identity')
				if ip != 503:
					ip = ip["ip_address"]
					ips.append(ip)
			print(ips)
		if resource == "hostnames":
			hostnames = []
			for vmid in vmids:
				hostname = get(f'/api/vcenter/vm/{vmid}/guest/identity')
				if hostname != 503:
					hostname = hostname["host_name"]
					hostnames.append(hostname)
			print(hostnames)
		if resource == "vlan":
			print('not implemented')